# A Naïve Bayes Spam Filter
This project contains a somewhat simple spam filter for text messages using a
naïve bayes approach to classify messages either as spam nor ham (non-spam).

The classifier is trained on a set of English message text messages and is thus 
primarily suited for classifying these as being spam or innocent.

In addition to classifying the messages from the initial dataset, there are
utilities available to classify further messages. The most useful one is 
probably the `R/classify_spam.R` script which can be called from the command
line and takes a message string as input. It will then output its classification
based on the known probabilities. 

## Performance
Against the training data set the filter achieves an accuracy of 0.986 and
a Kappa of 0.94, which seems pretty good for this relatively simple approach. 

## The Data
The dataset used is the UCI Machine Learning SMS Spam classification dataset,
which is available at: https://www.kaggle.com/uciml/sms-spam-collection-dataset

There is also the original website including links to published papers at
http://www.dt.fee.unicamp.br/~tiago/smsspamcollection/

## Usage
To use the spam filter you will first need to download the data and put 
the file `spam.csv` in the directory `data-raw` on the top level on the
repository. Then source the `R/runAll.R` to run the building of the vocabulary
and the initial classification against a split of the dataset. 

The `R/00_setup.R` file does contain the setting of a non-random seed for 
reproducibility (and it also puts out a message that it does), so you might 
want to remove this beforehand. 

As for building and updating vocabulary there are two functions here:
`rebuild_filter` which (re-)builds the entire vocabulary and `update_filter`
to add data from individual messages to an already exiting vocabulary.

To classify messages from the command line you can use 
`classify_spam.R MESSAGE` and you will get a result of ham/spam/unknown. 
As for updating the filter from the command line use 
`learn_message.R MESSAGE ham|spam vocab_old.Rds vocab_new.Rds`, with choosing
one of ham/spam an replacing the file names with what is actually on disk.

## Licence
All code in this repository is available under the Creative Commons BY-NC-SA
4.0 Licence, see file LICENCE or
https://creativecommons.org/licenses/by-nc-sa/4.0/.
